%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%           Congresso nazionale di Matematica 2021
%
%         Matematica 2021. Nuove proposte didattiche
%
%                     12, 13 novembre
%
%                         Verona
%
%------------------------------
% Compilazione:
% pdflatex --shell-escape congresso_21_atti.tex
%------------------------------
%
% This work may be distributed and/or modified under the
% conditions of the Creative Commons BY-SA 3.0
%
% Authors: Daniele Zambelli
%
% The Current Maintainer of this work is 
% Daniele Zambelli - daniele.zambelli@gmail.com
%
% Copyright 2020-24 Daniele Zambelli
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%========================
% Classe del documento
\documentclass[a4paper,10pt]{article}

%========================
% Lettura file esterni
\input{congresso_21_preambolo}
\input{congresso_21_funzioni}

%========================
% Variabili
\title{Matematica Dolce,\\ 
un libro di testo fuori dalla \emph{moda}}
\author{Daniele Zambelli\footnote{Curatore del progetto 
(\href{mailto:daniele.zambelli@gmail.com}{daniele.zambelli@gmail.com})}}
\date{13 novembre 2021}
%========================
% Contenuti
\begin{document}

\maketitle

\begin{abstract}
La \emph{moda} di una distribuzione di frequenza è la 
modalità caratterizzata dalla massima frequenza.
Le case editrici devono fare proprie le modalità di presentazione dei 
vari argomenti che sono più frequenti tra gli insegnanti, 
d'altra parte gli insegnanti sono portati a adeguarsi al libro di testo 
che hanno usato, 
così il sistema scuola tende fortemente a stabilizzarsi.
Presentare gli argomenti in modalità diverse da quelle standard richiede 
la realizzazione di nuovi strumenti didattici.

Da quasi dieci di anni ha incominciato a evolversi un libro di testo di 
matematica per le scuole superiori: un testo fuori dalla \emph{moda}.

Il testo non è ``bello'', non è ``completo'', contiene errori;
ciò nonostante può essere uno strumento utile per gli insegnanti che:
\begin{itemize} [noitemsep]
\item oltre a insegnare amano \emph{imparare};
\item cercano il \emph{confronto} con altri;
\item sono attenti alle \emph{cause delle difficoltà} dei propri alunni;
\item usano \emph{metodi che non sono supportati} dalle case editrici;
\item si prendono \emph{cura} dei propri strumenti di lavoro;
\item sono \emph{critici} nei confronti degli strumenti didattici che 
stanno usando;
\item \dots
\end{itemize}

In questo intervento verrà brevemente illustrato un progetto:
\begin{itemize} [noitemsep]
\item libero,
\item collaborativo,
\item evolutivo,
\item polimorfo,
\item accessibile.
\end{itemize}

\end{abstract}

\section{Introduzione}
\begin{flushright}
\emph{Parafrasando quello che dicono alcuni miei amici buddisti, \\
``La matematica è fatta di non matematica''.}
\end{flushright}

Tutto il progetto relativo alla costruzione di un manuale di matematica 
non è fatto solo di aspetti legati alla matematica e alla sua didattica. 
Gli aspetti matematico-didattici sono quelli che mi-ci interessano di più, 
ma in questa brevissima comunicazione non è possibile un dialogo e un 
confronto. 
In questi pochi minuti cercherò di illustrare gli \emph{aspetti non 
matematici} relativi al progetto \md\ e accennerò a 
quelli matematici solo come mini-esempi.

\section{Libertà}
\begin{flushright}
\begin{minipage}{.30\textwidth}
 \emph{Le 3 C di Imparare:
 \begin{itemize} [nosep]
  \item Copiare,
  \item Capire,
  \item Cambiare.
 \end{itemize}}
\end{minipage}
\end{flushright}

Tutto il progetto è realizzato sotto la licenza CC BY-SA e questo vuol dire 
che è possibile:
\begin{itemize} [noitemsep]
\item usare il materiale per qualunque scopo;
\item farne delle copie e diffonderlo;
\item modificarlo a proprio piacere e diffondere la versione modificata;
\item usarlo per ricavarne degli utili diretti o indiretti.
\end{itemize}

Ci sono due restrizioni:
\begin{itemize} [noitemsep]
\item deve essere citata la fonte originale del progetto;
\item deve essere mantenuta la licenza.
\end{itemize}

La scelta di questa licenza:
\begin{itemize} [noitemsep]
\item rende libero l'insegnante di usare le parti che 
gli aggradano e modificare quelle che non si addicono al suo stile di 
insegnamento;
\item permette alla scuola di far stampare il testo e di richiedere un 
contributo agli alunni.
\end{itemize}

Per una descrizione più precisa della licenza vedi:

{\small
\url{https://creativecommons.org/licenses/by-sa/3.0/it/}

\url{https://creativecommons.org/licenses/by-sa/3.0/it/legalcode}
}

\subsection{Esempio: Piano cartesiano} 
Per realizzare \md\ sono partito dal testo
``Matematica \(C^3\)'', 
scaricabile da \quad 
\url{www.matematicamente.it/manuali-scolastici/},
rilasciato con una licenza libera, quindi modificabile e distribuibile 
liberamente.

Il gruppo che curava l'adattamento del testo alle esigenze della nostra 
scuola ha deciso di aggiungere nel primo volume un capitolo sul piano 
cartesiano.
% che non era presente in ``Matematica \(C^3\)''.
In questo primo capitolo sono presenti i due classici problemi sul 
segmenti:
% , ma è stato aggiunto anche un terzo problema che non si trova, 
% di solito nei testi:

% Questo testo non presentava, nel biennio, il piano cartesiano, 
% ma, avendo una licenza libera, il gruppo che curava l'adattamento del 
% testo alle esigenze della nostra scuola ha potuto modificarlo. 
% Abbiamo inserito, nel primo volume, un capitolo sul piano cartesiano con i 
% problemi sui segmenti:

\noindent \begin{minipage}[t]{.36\textwidth}
\begin{center}
Punto medio

\scalebox{.6}{\puntomedio}
\end{center}
\end{minipage}
\begin{minipage}[t]{.33\textwidth}
\begin{center}
Lunghezza

\scalebox{.6}{\lungseg}
\end{center}
\end{minipage}
\begin{minipage}[t]{.33\textwidth}
\begin{center}
Area sottesa

\scalebox{.6}{\areasottesauno}
\end{center}
\end{minipage}

Ma ne è stato aggiunto un terzo che, di solito, non viene proposto nei 
libri di testo: calcolare l'area sottesa ad un segmento.
% I primi due problemi si trovano in tutti i libri, il terzo è 
% particolarmente 
% interessante: calcolare l'area sottesa ad un segmento.

Questo esercizio, dal lato rivolto al \emph{passato}, recupera conoscenze 
che gli alunni hanno dalle elementari,
ma le recupera rompendo uno schema fortemente impresso nella loro mente: 
il trapezio è stato sempre presentato loro con le basi orizzontali 
e l'altezza verticale.

Dal lato rivolto al \emph{futuro} propone un problema che verrà affrontato 
pienamente negli ultimi anni della scuola superiore, ma ne propone un caso 
particolare affrontabile con le competenze che gli alunni hanno già al 
primo anno.

Permette, inoltre, di riflettere sulle \emph{convenzioni} e sul significato 
dei termini usati, dato che il concetto di ``area sottesa'' ha sì delle 
analogie, ma, a differenza dell'``area di una figura'', può anche 
essere negativa.

\section{Cooperazione}
\begin{flushright}
\emph{La libertà non è star sopra un albero...\\
... libertà è partecipazione.\\
Giorgio Gaber
}
\end{flushright}


Il testo non è il frutto del lavoro di una sola persona, molti vi hanno 
collaborato.
Il confronto con i colleghi sulle scelte didattiche è sempre un momento di 
grande arricchimento.
Inoltre difficilmente un insegnante, da solo, può affrontare la 
scrittura di un intero manuale di matematica, ma, assieme ad altri è 
possibile farlo.

Avendo a disposizione una base sufficientemente solida che copre gli 
argomenti dei cinque anni dell'insegnamento secondario, ogni insegnante 
può aggiungere o modificare il proprio argomento del cuore, 
può proporre un modo più efficace di trattare un argomento e 
renderlo disponibile a tutti.

Ma lavorare assieme a un progetto non è facile, ci sono diversi aspetti 
problematici:
\begin{enumerate} 
\item \emph{Aspetti non tecnici:}
\begin{enumerate} [nosep]
\item serve una certa \emph{sicurezza};
\item serve la \emph{disponibilità} al confronto e alla condivisione.
\end{enumerate}
\item \emph{Aspetti tecnici:}
\begin{enumerate} [nosep]
\item servono degli \emph{strumenti comuni};
\item serve una \emph{struttura che permetta lo scambio} di materiali.
\end{enumerate}
\end{enumerate}

Il primo aspetto, è quello più difficile, ma su questo non ho competenze 
che mi permettano di scrivere qualcosa di sensato.
Per quanto riguarda lo strumento con cui è realizzato il testo è 
stato scelto: \LaTeX.
% \hspace*{\fill}\LaTeX.\hspace*{\fill}

Molte persone che hanno una formazione matematica conoscono 
\LaTeX, per chi non lo conoscesse, in sintesi:
\begin{itemize} [noitemsep]
\item \LaTeX\ è un \emph{linguaggio} di descrizione del testo; 
\item ha un ottimo compositore di \emph{formule};
\item permette di realizzare \emph{grafici anche sofisticati};
\item chi scrive si concentra sui contenuti lasciando a \LaTeX\ la 
composizione;
\item produce documenti con una qualità professionale;
\item è adatto anche a documenti grandi e complessi.
\end{itemize}

% Molte persone che hanno una formazione matematica conoscono 
% \LaTeX, per chi non lo conoscesse, in sintesi:
% \begin{itemize} [noitemsep]
% \item \LaTeX\ è un \emph{linguaggio di descrizione del testo}; 
% \item permette di scrivere tutte le \emph{formule} usando un testo piano;
% \item permette di realizzare \emph{grafici anche sofisticati};
% \item può produrre documenti in formato \emph{.pdf} o 
% in \emph{altri formati}.
% \end{itemize}

Anche \LaTeX\ è libero, lo si può scaricare, gratuitamente, e installare 
su qualunque piattaforma.
È anche possibile usarlo attraverso un browser usando servizi messi a 
disposizione in rete senza doverlo installare 
(ad es. \url{www.overleaf.com}). 
 

% Si usa un editor per descrivere, in un semplice file di testo, 
% il documento che si vuole realizzare, lo si compila e si ottiene un 
% documento .pdf di buona (ottima) qualità.

% \medskip
La struttura usata per la condivisione del lavoro verrà presentata nella 
prossima sezione, dato che riguarda anche la manutenzione del testo.

\subsection{Esempio: disequazioni goniometriche} Discutendo con le colleghe 
sull'argomento equazioni e 
disequazioni goniometriche era sorta una divergenza: alcune preferivano 
usare la \emph{circonferenza goniometrica}, 
altre il \emph{grafico della funzione}.

La soluzione adottata è stata quella di combinare in un unico disegno 
circonferenza e grafico: 

\begin{center}
\disequazionegon
\end{center}

Questa combinazione non appesantisce particolarmente la trattazione e 
permette all'insegnante di adottare il metodo che predilige e, all'alunno, 
di scegliere il modo che più gli si confà.

\section{Evoluzione}
\begin{flushright}
\emph{La ricerca della conoscenza \\
non si nutre di certezze:\\
si nutre di una radicale\\
assenza di certezze.\\
Carlo Rovelli}
\end{flushright}

Non esiste il modo \emph{giusto} per insegnare;
esistono vari modi per ogni argomento, 
modi che possono adattarsi più o meno allo \emph{stile di apprendimento} 
dell'insegnante e degli alunni. 

Esistono insegnanti che non riescono a fare lezione sempre allo stesso 
modo: a volte, stimolati dal contesto, si trovano metodi o esempi che sono
più efficaci di altri; inserirli nel libro di testo, vuol dire conservarli e 
tramandarli a altri.

Il libro può non piacere e certamente non è \emph{perfetto} (per fortuna), 
ma evolve facilmente e ogni lezione svolta in classe può suggerire modifiche 
piccole o grandi, come 
la \emph{correzione} di un errore, 
la \emph{modifica} o l'\emph{aggiunta} di un esempio o di un esercizio, 
la \emph{riscrittura} di alcune parti o di un intero capitolo.

% Il libro può non piacere e certamente non è \emph{perfetto} 
% (per fortuna), ma evolve facilmente e ogni lezione svolta in classe 
% può suggerire modifiche piccole o grandi: 
% \begin{itemize} [noitemsep]
% \item la \emph{correzione} di un errore, 
% \item il \emph{cambiamento} o l'aggiunta di un esempio, 
% \item la \emph{riscrittura} di alcune parti o di un intero capitolo.
% \end{itemize}

Mantenere un manuale in continua evoluzione non è semplice, in 
\md\ viene utilizzato uno degli strumenti usati nello 
sviluppo del software il \emph{Sistema di Controllo di Versione} (CVS).

In estrema sintesi consiste nell'avere l'intero lavoro sia in \emph{locale}, 
cioè nell'hard disk del proprio computer dove è possibile modificarlo, sia 
in un repository \emph{remoto} dove può essere condiviso. 

Un apposito programma, \emph{git}, fornisce gli strumenti per 
sincronizzare lo stato locale e quello remoto e tiene traccia di tutti 
i cambiamenti in modo da poter tornare indietro a qualunque stadio della 
lavorazione, e di gestire gli eventuali conflitti di un lavoro svolto da 
più persone.


\subsection{Esempio: Funzioni} 

Nelle prime edizioni di \md\ le \emph{funzioni} venivano introdotte dopo 
aver trattato: 
\emph{insiemi}, \emph{prodotto cartesiano} e \emph{relazioni}.
A un certo punto, la contaminazione della matematica con 
l'\emph{informatica} ha suggerito un nuovo approccio che ha permesso di 
introdurre le funzioni già nelle prime pagine del primo volume.

\emph{Una funzione è un qualunque ``procedimento'' che dà un risultato}. 
La funzione può avere uno o più \emph{parametri} ai quali, nel momento 
dell'esecuzione, devono essere associati dei valori detti \emph{argomenti}.

Questa definizione, che verrà precisata nel prosieguo degli studi, 
è diretta, non si basa su altri concetti poco intuitivi e può riferirsi 
a esperienze già acquisite dagli alunni come le operazioni aritmetiche o, 
a funzioni con un solo parametro come il \emph{doppio}, la \emph{metà}, 
il \emph{quadrato}, \dots

\vspace{.5em}
\affiancatic{.49}{.49}{
Funzione binaria generica
\[fun \coppia{a_1}{a_2} = risultato\]
% \hspace*{-6mm} 
\scalebox{.8}{\grafoscatolasd{fun}{a_1}{a_2}{risultato}}\\[.5em]
\scalebox{.8}{\grafoportad[1.1]{fun}{a_1}{a_2}{risultato}}
}{
Chiamata di una funzione
\[add \coppia{7}{5} = 12\]
% \hspace*{-6mm} 
\scalebox{.8}{\grafoscatolasd{add}{7}{5}{12}}\\[.5em]
\scalebox{.8}{\grafoportad[1.1]{+}{7}{5}{12}}
}

% \vspace{.5em}
\section{Polimorfismo}

\begin{flushright}
\emph{Un altro \emph{modo} è possibile.}
\end{flushright}

\begin{description}
\item [Problema: ] <<Mi va abbastanza bene in generale, ma quell'argomento 
è trattato in un modo che non risponde alle mie esigenze>>.
\item [Soluzione: ] seguo le istruzioni:
\begin{enumerate} [noitemsep]
\item \emph{opero un \emph{fork}} del progetto;
\item \emph{modifico} quello che non mi va bene;
\item \emph{propongo al gruppo} di integrare nel testo le modifiche.
\end{enumerate}
\end{description}

\emph{Se le proposte di modifica vengono accolte}, 
il cambiamento viene integrato nel testo.

\emph{Se le proposte di modifica non vengono accolte}, 
si tengono due testi con una radice comune e alcune parti diverse.

\md\ rende facile l'operazione di adattare il testo 
alle proprie esigenze:
\begin{itemize} [noitemsep]
\item si può \emph{cambiare l'ordine dei capitoli};
\item si possono \emph{aggiungere o togliere argomenti};
\item si possono \emph{modificare argomenti};
\item si possono \emph{aggiungere (o togliere) esercizi}.
\end{itemize}

\subsection{Esempio 1: Modifica della struttura}

Senza modificare i contenuti è possibile ottenere testi diversi, si può:
\begin{itemize} [noitemsep]
\item \emph{Estrarre un solo capitolo}, da usare come dispensa, 
non richiede più di~5 minuti.
\item \emph{Trasformare il testo da ``5-volumi'' a ``moduli''} dedicati 
ai diversi argomenti.
\item \emph{Spostare argomenti} da un volume all'altro per adattare il testo 
alle esigenze della scuola (e non adattare la programmazione al libro 
adottato).
\end{itemize}

\subsection{Esempio 2: Analisi Non Standard}

L'analisi matematica proposta nel quinto volume è trattata con 
il metodo \emph{non standard}. 
% Il quinto volume è dedicato principalmente all'analisi matematica 
% proposta con il metodo \emph{non standard}, 
È così, perché chi ha scritto questi capitoli preferisce, e 
ha usato nella sua attività didattica, l'\emph{analisi non standard}.

\begin{center}
\textbf{Numeri Iperreali}
\end{center}

Questo modo di affrontare l'analisi può partire dalla seguente 
osservazione:\\
se tra due classi contigue di razionali scegliamo di mettere:

\bigskip
\affiancati{.48}{.48}{
\emph{un solo separatore:}
}{
\emph{più separatori:}
}

\affiancati{.48}{.48}{
\begin{center}\scalebox{.6}{\unoconnome}\end{center}
}{
\begin{center}\scalebox{.6}{\molticonnome}\end{center}
}

\affiancati{.48}{.48}{
questa scelta, e le sue conseguenze, ci permette di ottenere un nuovo insieme 
numerico: l'insieme \emph{completo} dei \emph{numeri reali}: \(\R\).
}{
% \vspace*{-1mm}
questa scelta, e le sue conseguenze, ci permette di ottenere un nuovo 
insieme numerico (che contiene anche i numeri infinitesimi e i loro 
reciproci infiniti): il meraviglioso insieme dei 
\emph{numeri iperreali}: \(\IR\).
% 
% \affiancati{.48}{.48}{
% otteniamo dei nuovi numeri che, aggiunti ai razionali, formano l'insieme 
% \emph{completo} dei \emph{numeri reali}: \(\R\).
% }{
% % \vspace*{-1mm}
% otteniamo dei nuovi numeri, tra cui gli infinitesimi e i loro reciproci 
% infiniti, che formano il meraviglioso insieme dei 
% \emph{numeri iperreali}: \(\IR\).
}

\bigskip
Nei disegni, i cerchietti rappresentano dei \emph{microscopi non standard} 
cioè con ingrandimento infinito: \(\times \infty\).

Poiché nell'insieme degli \emph{iperreali} ci sono anche \emph{infinitesimi} 
e \emph{infiniti} questo insieme è particolarmente adatto a 
risolvere i problemi relativi alla pendenza delle funzioni e alle aree 
delle superfici sottese ai loro grafici.

\begin{center}
\textbf{Pendenza di una funzione in un punto}
\end{center}

\affiancati{.54}{.44}{
In una funzione abbastanza ``tranquilla'', la \emph{pendenza} in un punto 
è il numero reale più vicino al rapporto differenziale:
\[f'(x) \approx \dfrac{df(x_0)}{dx} = 
  \dfrac{f(x_0 + \epsilon) - f(x_0)}{\epsilon}\]
dove \(\epsilon\) è un qualunque infinitesimo non nullo.
}{
\scalebox{.6}{\differenziale}
}

\pagebreak
\begin{center}
\textbf{Teorema fondamentale del calcolo differenziale}
\end{center}

\affiancati{.48}{.48}{

% \vspace{-2mm}
\hspace*{+2mm}
\scalebox{.6}{\teoremafonda}

\vspace{5mm}
L'area \(\Delta S\) sottesa al grafico 
di \(f(x)\)
nell'intervallo reale
\(\Delta x\) è \emph{approssimativamente}:
\(f(x) \cdot \Delta x\).\\
Questo permette di approssimare a piacere l'area sottesa.
\vspace{3mm}
}{
\hspace*{-22mm}\scalebox{.6}{\teoremafondb}

L'area \(d S \) sottesa alla funzione nell'intervallo infinitesimo
\(d x\) è \emph{indistinguibile} da: 
\(f(x) \cdot d x\).\\
Da questo si ricava che \(f(x)\) è indistinguibile, nei reali, dalla 
derivata della funzione dell'area sottesa:\\ [.3em]
\hspace*{1.5mm}
\(d S \sim f(x) \cdot d x \sRarrow S' \sim \dfrac{d S}{dx} \sim f(x)\).
}

\subsection{Esempio 3: E per gli analisti standard?}

Ma non tutti gli insegnanti sono stati rapiti 
dall'\emph{analisi non standard}! 
Nessun problema: 
grazie alla \emph{licenza adottata} e ai \emph{sorgenti a disposizione},
chi non volesse usare questa modalità, può modificare alcuni capitoli 
iniziando l'analisi dalla 
\emph{definizione ``\(\epsilon - \delta\)'' di limite} e ottenendo così 
un testo adatto ai propri gusti.
In questo modo il progetto si arricchisce e si adatta a diverse esigenze.

\section{Accessibilità}

\begin{flushright}
\emph{Écoutez bien\\
pour mieux comprendre.\\
Thich Nhat Hanh}
\end{flushright}

L'\emph{apparato grafico} ha grande importanza nella didattica della 
matematica.
Gli argomenti sono spesso illustrati con grafici e, quando possibile, sono 
stati proposti metodi di soluzione grafici.
Ad esempio il calcolo di espressioni in \(\N\) e la scomposizione in 
fattori sono proposti con \emph{grafi ad albero}.

\subsection{Axessibility}

Ma i \emph{metodi grafici}, se aiutano alcuni alunni, non sono 
utilizzabili da chi ha \emph{difficoltà visive} e può ``guardare'' 
il testo solo attraverso una barra braille.

Ciechi e ipovedenti hanno grande difficoltà nello studio della matematica 
in particolare nella lettura delle formule.

\md, grazie alla licenza e all'uso di \LaTeX, ha 
integrato il pacchetto \emph{axessibility} sviluppato 
dall'università di Torino che permette ai lettori vocali di pdf di leggere 
le formule nel formato \LaTeX, e quindi in un formato comprensibile.

Il sito di riferimento è:

\begin{center}
\url{www.integr-abile.unito.it/axessibility}
\end{center}

\subsection{Lambda}

È in corso la traduzione in formato \emph{html} con le formule 
in \emph{Mathjax} visibili sia in \emph{\LaTeX} sia in \emph{mathml}. 
Anche questo formato facilita la fruizione da parte di chi ha problemi di 
vista.

Da questi formati si potrà tradurre \md\ in \emph{Lambda}.
\emph{Lambda} è un programma che facilita le interazioni tra studente 
disabile, testi di matematica e docente.

% \bigskip
Il sito di riferimento è:

\begin{center}
\url{www.lambdaproject.org}
\end{center}

Le soluzioni tecniche non sono, comunque, la panacea ai problemi di 
accessibilità dei materiali. 
Il lavoro dell'insegnante è quello di trovare metodi che siano inclusivi, 
che possano essere utilizzati anche da chi ha delle disabilità.

Dato che le disabilità sono tante, ognuno di noi ha le proprie disabilità, 
non si può mai considerare terminata la ricerca di metodi che siano 
accessibili a un più largo numero di alunni.

\subsection{Esempio} Già da quando insegnavo alle medie usavo un grafo ad 
albero per la soluzione delle espressioni con i numeri naturali. 
I motivi di questa scelta sono vari:

\begin{itemize} [noitemsep]
\item permette di separare il \emph{problema della precedenza} delle 
operazioni dal \emph{problema del calcolo};
\item è un esercizio significativo nell'uso dei \emph{grafi ad albero};
\item aggiunge lo strumento \emph{grafi} alla ``cassetta degli attrezzi'' 
degli alunni;
\item \emph{evita le ricopiature} di una parte dell'espressione;
\item \emph{permette di risalire} a un elemento mancante dell'espressione 
conoscendone il risultato;
\item produce una \emph{soluzione uguale} per tutti a meno di banali 
trasformazioni topologiche;
\item \emph{dà importanza all'aspetto grafico};
\item è bello.
\end{itemize}

Di seguito riporto un esempio di espressione risolta con un grafo ad albero.

\espressionealberoa

Proprio gli ultimi due punti delle motivazioni precedenti evidenziano il 
conflitto con chi ha problemi di vista.
Un testo che voglia essere inclusivo deve offrire una soluzione alternativa 
che permetta di mantenere almeno i più importanti obiettivi.
Viene proposto quindi un secondo metodo che può essere eseguito anche su una 
barra braille.
Il metodo consiste nella ripetizione di due fasi:

\begin{enumerate} [noitemsep]
\item evidenziare tutte le operazioni che si possono svolgere;
\item sostituire a queste operazioni il loro risultato.
\end{enumerate}

Di seguito l'applicazione del metodo alla precedente espressione.

% \[2 + 6 \times 2 \div 
%  \left[ \left(4 -2 \right) \times 3^{2} - 3 \times 5 \right] +
%  \left( 5^{2} + 2^{3} \right) \div 3 =\]
\begin{align*}
\text{Sottolineo:} \qquad & 2 + 
 \underline{6 \times 2} \div \left[ \underline{\left(4 -2 \right)} 
   \times \underline{3^{2}} - 
   \underline{3 \times 5} \right] +
 \left( \underline{5^{2}} + \underline{2^{3}} \right) \div 3 =\\
\text{Eseguo: } \qquad &= 2 + 
 12 \div \left[ 2 \times 9 - 15 \right| +
 \left( 25 + 8 \right) \div 3 =\\
\text{Sottolineo: } \qquad &= 2 + 
 12 \div \left[ \underline{2 \times 9} - 15 \right| +
 \underline{\left( 25 + 8 \right)} \div 3 = \hspace{20mm}\\ 
\text{Eseguo: } \qquad &= 2 + 12 \div \left[ 18 - 15 \right| +
 33 \div 3 =\\ 
\text{Sottolineo: } \qquad &= 2 + 
 12 \div \underline{\left[ 18 - 15 \right|} +
 \underline{33 \div 3} = \\ 
\text{Eseguo: } \qquad &= 2 + 
 12 \div 3 +
 11 = \\ 
\text{Sottolineo: } \qquad &= 2 + 
 \underline{12 \div 3} +
 11 = \\ 
\text{Eseguo: } \qquad &= 2 + 
 4 +
 11 = \\ 
\text{Sottolineo: } \qquad &= \underline{2 +  4} + 11 = \\ 
\text{Eseguo: } \qquad &= 6 + 11 = 17
\end{align*}

Da notare che ho distinto le due fasi su due righe diverse solo per 
chiarezza dell'esposizione, in pratica la lunghezza della soluzione si 
dimezza:
\begin{align*}
& 2 + 
 \underline{6 \times 2} \div \left[ \underline{\left(4 -2 \right)} 
   \times \underline{3^{2}} - 
   \underline{3 \times 5} \right] +
 \left( \underline{5^{2}} + \underline{2^{3}} \right) \div 3 =\\
&= 2 + 
 12 \div \left[ \underline{2 \times 9} - 15 \right| +
 \underline{\left( 25 + 8 \right)} \div 3 = \hspace{20mm}\\ 
&= 2 + 
 12 \div \underline{\left[ 18 - 15 \right|} +
 \underline{33 \div 3} = \\ 
&= 2 + 
 \underline{12 \div 3} +
 11 = \\ 
&= \underline{2 +  4 + 11} = 17
\end{align*}


\section{Conclusioni}

\begin{flushright}
\emph{
giusto!\\
nel verso\\
forse è perché non guardiamo le cose\\
Quando non ci capiamo,}
\end{flushright}

% \subsection{Buoni motivi per non adottare Matematica Dolce}

\subsection{Motivi per non adottare Matematica Dolce}

Questo testo non è adatto a tutte le situazioni, le stesse caratteristiche 
che possono essere considerate interessanti da qualche insegnante, possono 
essere viste da altri come degli inaccettabili difetti.

\begin{description} [noitemsep]
\item [È fuori dalla moda: ] l'insegnante deve essere convincente nei 
confronti degli alunni sospettosi verso i metodi diversi da quelli usati 
dai loro amici, deve far fronte ai genitori che non sanno dove mandare a 
``lezione'' il figlio che non ha capito e deve resistere ai colleghi che 
non ne vogliono sapere dell'introduzione di metodi strani nella scuola.
\item [È libero: ] questo comporta una certa responsabilità di chi 
lo usa:
se c'è qualcosa che non va deve contribuire a correggerlo segnalando o 
mandando le correzioni.
\item [È collaborativo: ] presenta una disomogeneità tra i vari capitoli.
\item [È evolutivo: ] la versione di quest'anno è diversa, in certi punti 
poco, in certi molto, da quella dell'anno precedente.
\item [È polimorfo: ] chi lo adotta deve decidere se il testo va bene così 
com'è o se va adattato o modificato.
\item [È accessibile: ] questo responsabilizza maggiormente 
l'insegnante che, se ha un alunno disabile, deve interessarsi alle 
tecnologie assistive utilizzabili con il testo.
\end{description}

\subsection{Contribuire}

Se, nonostante tutto, qualcuna delle caratteristiche di questo progetto 
risuona con la propria visione dell'insegnamento o della matematica, allora 
sorge la domanda: come posso collaborare?
Ci sono molti modi per contribuire a \md:

\begin{description} [noitemsep]
\item [Usarlo: ] il libro è vivo se viene usato.
\item [Correggerlo: ] il libro è vivo se vengono segnalati e corretti 
gli errori.
\item [Adattarlo: ] il libro è vivo se si adatta a diversi 
ambienti e a nuove situazioni.
\item [Ampliarlo: ] il libro è vivo se cresce.
\item [Renderlo accessibile: ] l'inclusività è un obiettivo 
fondante del progetto.
\end{description}

\subsection{Informazioni}

Una buona partenza per collegarsi al progetto è il sito:

% \bigskip
\begin{center}
\href{https://www.matematicadolce.eu}
     {{\huge www.matematicadolce.eu}}
\end{center}

% \bigskip
Dove si possono trovare:
\begin{itemize}
\item i \emph{link per scaricare} l'ultima versione 
(\emph{\textsc{downloads}}),
\item i \emph{contatti} con chi cura il progetto 
(\emph{\textsc{chi siamo}}),
\item alcune indicazioni sulle \emph{scelte didattiche}
(\emph{\textsc{perché?}}),
\item \emph{Questa presentazione}
(\emph{\textsc{varie}}).
\end{itemize}

\section*{Sitografia}

Qualche indicazione su progetti analoghi a \md.

\bigskip
Altri progetti liberi e collaborativi: 

\url{it.wikipedia.org}

\url{it.wikiversity.org}

\url{it.m.wikibooks.org}

In particolare:

\url{it.wikibooks.org/wiki/Wikibooks:Biblioteca\_scolastica}

\bigskip
Un progetto collaborativo ma non libero:

\url{www.bookinprogress.org}

\bigskip
Siti da cui si possono scaricare testi di matematica:

\url{mathematica.sns.it}

\url{www.openculture.com/free-math-textbooks}

\url{www.free-ebooks.net/mathematics-textbooks}

\url{e-booksdirectory.com/mathematics.php}

% \url{http://bibliotecamatematica.cab.unipd.it/cosa-cerchi/contenuti-cosa-
% cerchi/libri-digitalizzalizzati}

\end{document}















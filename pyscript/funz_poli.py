#!/usr/bin/python3
# -*- coding: utf-8 -*-
#-------------------------------python---------------------funzpoli.py--#
#                                                                       #
#          Calcola i valori di una funzione polinomiale                 #
#                                                                       #
#--Daniele Zambelli--------------GPL------------------------------2022--#

"""
calcola i valori di una funzione polinomiale.

"""
import math

def valpoli(nomef, coeff, x):
    valore = 0
    for coe in coeff:
        valore = valore * x +coe
    res = rf'\(V({x}) = +4 \cdot {x}^3 -10 \cdot {x}^2 +6 \cdot {x} = {valore}\)'
    return res.replace('.', ',')

print(valpoli('V', (4, -10, 6, 0), .35))
print(valpoli('V', (4, -10, 6, 0), .45))

print()

print(valpoli('V', (4, -10, 6, 0), (5-7**.5)/6))
print(valpoli('V', (4, -10, 6, 0), (5+7**.5)/6))

print()

for x in range(1, 10):
    print(valpoli('V', (4, -10, 6, 0), .3+x/100))

print()

print(valpoli('V', (4, -10, 6, 0), .385))
print(valpoli('V', (4, -10, 6, 0), .395))

print()

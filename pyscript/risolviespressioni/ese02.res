\item \((-a-1-2a)-(-3-a+4a)\) 
 \result{2 - 6 a}
\item \(\tonda{2a^{2}-3b}-\quadra{\tonda{4b+3a^{2}}-\tonda{a^{2}-2b}}\) 
 \result{- 9 b}
\item \(\tonda{2a^{2}-5b}-\quadra{\tonda{2b+4a^{2}}-\tonda{2a^{2}-2b}}+9b\) 
 \result{0}
\item \(3a\quadra{2(a-2ab)+3a\tonda{\dfrac{1}{2}+3b}-\dfrac{1}{2}a(3-5b)}\) 
 \result{\dfrac{45 a^{2} b}{2} + 6 a^{2} + \dfrac{5 a b}{2} - \dfrac{3 a}{2}}
\item \(2(x-1)(3x+1)-\tonda{6x^{2}+3x+1}+2x(x-1)\) 
 \result{4 x^{2} - 11 x - 3}
\item \(\tonda{\dfrac{1}{3}x-1}(3x+1) - 2x\tonda{\dfrac{5}{4}x - 
\dfrac{1}{2}}(x+1) - \dfrac{1}{2}x\tonda{x-\dfrac{2}{3}}\) 
 \result{- \dfrac{5 x^{3}}{2} - x^{2} - \dfrac{4 x}{3} - 1}
\item \(\tonda{a-b}\tonda{a+b} + \tonda{a+b}\tonda{ab-a} - 
\tonda{ab+a}^2 + ab\tonda{ab+a}\) 
 \result{- a^{2} + a b^{2} - a b - b^{2}}
\item \(ab\tonda{a^{2} - b^{2}} + 2b\tonda{x^{2} - a^{2}}(a-b) - 
2bx^{2}(a-b)\) 
 \result{- a^{3} b + 2 a^{2} b^{2} - 2 a^{2} b x^{2} - a b^{3} + 4 a b^{2} x^{2} - 2 b^{3} x^{2}}
\item \(2y \tonda{\dfrac{3}{2}x^{2} - \dfrac{1}{2}x} -
        \tonda{2xy - \dfrac{1}{3}y} 4x + \tonda{5x+2}\tonda{xy-3}\) 
 \result{\dfrac{7 x y}{3} - 15 x - 6}
\item \(\tonda{\dfrac{1}{2}a-\dfrac{1}{2}a^{2}}(1-a)\quadra{a^{2}+2a - 
\tonda{a^{2}+a+1}}\) 
 \result{\dfrac{a^{4}}{2} - \dfrac{3 a^{3}}{2} + \dfrac{3 a^{2}}{2} - \dfrac{a}{2}}
\item \((1-3x)(1-3x)-(-3x)^{2}+5(x+1)-3(x+1)-7\) 
 \result{- 4 x - 4}
\item \(3\tonda{x-\dfrac{1}{3}y}\quadra{2x+\dfrac{1}{3}y-(x-2y)} - 
2\tonda{x-\dfrac{1}{3}y}(2x+3y)\) 
 \result{- 5 x^{2} - \dfrac{10 x y}{3} + \dfrac{5 y^{2}}{3}}
\item \(\dfrac{1}{24}(29x+7)-\dfrac{1}{2}x^{2}+\dfrac{1}{2}(x-3)(x-3)-2 - 
\quadra{\dfrac{1}{3}-\dfrac{3}{2}\tonda{\dfrac{3}{4}x+\dfrac{2}{3}}}\) 
 \result{\dfrac{83}{24} - \dfrac{2 x}{3}}
\item \(-{\dfrac{1}{4}}\tonda{2 abx+2a^{2}b^{2}+3ax} 
+a^{2}(b^{2}+x^{2})-\quadra{\tonda{\dfrac{1}{3} ax}^{2} 
-\tonda{\dfrac{2}{3}bx}^{2}}+
\dfrac{1}{4}ax\tonda{2b+3}\) 
 \result{\dfrac{a^{2} b^{2}}{2} + \dfrac{8 a^{2} x^{2}}{9} + \dfrac{a b x}{2} + \dfrac{3 a x}{4} + \dfrac{4 b^{2} x^{2}}{9}}
\item \(\tonda{\dfrac{1}{3}x+\dfrac{1}{2}y-\dfrac{3}{5}}\tonda{\dfrac{1}{3}x- 
\dfrac{1}{2}y+\dfrac{3}{5}}-\quadra{\tonda{\dfrac{1}{3}x}^{2}- 
\tonda{\dfrac{1}{2}y}^{2}}\) 
 \result{\dfrac{3 y}{5} - \dfrac{9}{25}}
\item \(\tonda{\dfrac{1}{2}x-1}\tonda{\dfrac{1}{4}x^{2}+\dfrac{1}{2}x+1} + 
\tonda{ -{\dfrac{1}{2}}x}^{3}+2\tonda{\dfrac{1}{2}x+1}\) 
 \result{x + 1}
\item \((3a-2)(3a+2)-(a-1)(2a-2)+a(a-1)\tonda{a^{2}+a+1}\) 
 \result{a^{4} + 7 a^{2} + 3 a - 6}
\item \(-4x(5-2x)+\tonda{1-4x+x^{2}}\tonda{1-4x-x^{2}}\) 
 \result{- x^{4} + 24 x^{2} - 28 x + 1}
\item \(-(2x-1)(2x-1)+\quadra{x^{2}-\tonda{1+x^{2}}}^{2}-\tonda{x^{2} 
-1}\tonda{x^{2}+1}\) 
 \result{- x^{6} - 2 x^{4} - 3 x^{2} + 4 x + 2}
\item \(4(x+1)-3x(1-x)-(x+1)(x-1)-\tonda{4+2x^{2}}\) 
 \result{x + 1}
\item \(\dfrac{1}{2}(x+1)+\dfrac{1}{4}(x+1)(x-1) - \tonda{x^{2}-1}\) 
 \result{- \dfrac{3 x^{2}}{4} + \dfrac{x}{2} + \dfrac{5}{4}}
\item \((3x+1)\tonda{\dfrac{5}{2}+x} - (2x-1)(2x+1)(x-2)+2x^{3}\) 
 \result{- 4 x^{3} + 19 x^{2} + \dfrac{21 x}{2} - \dfrac{3}{2}}
\item \(\tonda{a-\dfrac{1}{2}b}a^{3} - 
\tonda{\dfrac{1}{3}{ab}-1}\quadra{2a^{2}(a-b) - a\tonda{a^{2}-2{ab}}}\) 
 \result{- \dfrac{a^{4} b}{3} + a^{4} - \dfrac{a^{3} b}{2} + a^{3}}
\item \(\tonda{3x^2+6xy-4y^2}\tonda{\dfrac{1}{2}xy - \dfrac{2}{3}y^2}\) 
 \result{\dfrac{3 x^{3} y}{2} + x^{2} y^{2} - 6 x y^{3} + \dfrac{8 y^{4}}{3}}
\item \((2a-3b)\tonda{\dfrac{5}{4}a^{2} + \dfrac{1}{2}{ab} - 
\dfrac{1}{6}b^{2}} - \dfrac{1}{6}a\tonda{12a^{2} - 
\dfrac{18}{5}b^{2}}+\dfrac{37}{30}ab^{2} - \dfrac{1}{2}a\tonda{a^{2} 
-\dfrac{11}{2}{ab}}\) 
 \result{\dfrac{b^{3}}{2}}
\item \(\dfrac{1}{3}xy\quadra{\tonda{x-y^{2}}\tonda{x^{2} -\dfrac{1}{2}y} - 
3x\tonda{-{\dfrac{1}{9}xy}}\tonda{3y}} - \dfrac{1}{3}x\tonda{x^{3}y + 
\dfrac{1}{4}xy^{2}}\) 
 \result{- \dfrac{x^{4} y}{3} - \dfrac{x^{2} y^{2}}{3} + \dfrac{x y^{4}}{6}}
item \graffa{\quadra{\tonda{-\dfrac{7}{2}}x+\dfrac{6}{3}}x-
         \dfrac{5}{4}}x+\dfrac{4}{5} 
 \result{- \dfrac{7 x^{3}}{2} + 2 x^{2} - \dfrac{5 x}{4} + \dfrac{4}{5}}
item (-a-1-2)-(-3-a+a) 
 \result{- a}
item \tonda{2a^{2}-3b}-\quadra{\tonda{4b+3a^{2}}-\tonda{a^{2}-2b}} 
 \result{- 9 b}
item \tonda{2a^{2}-5b}-\quadra{\tonda{2b+4a^{2}}-\tonda{2a^{2}-2b}}-9b 
 \result{- 9 b}
item 3a\quadra{2(a-2{ab})+3a\tonda{\dfrac{1}{2}-3b}-\dfrac{1}{2}a(3-5b)} 
 \result{3 a{\left(- 4 a b + 2 a + 3 a{\left(\dfrac{1}{2} - 3 b \right)} - \dfrac{a{\left(3 - 5 b \right)}}{2} \right)}}
item 2(x-1)(3x+1)-\tonda{6x^{2}+3x+1}+2x(x-1) 
 \result{- 7 x + 2 x{\left(x - 1 \right)} - 3}
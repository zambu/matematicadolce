item (-a-1-2)-(-3-a+a) 
 \result{- a}
item (2a^{2}-3b)-[(4b+3a^{2})-(a^{2}-2b)] 
 \result{- 9 b}
item (2a^{2}-5b)-[(2b+4a^{2})-(2a^{2}-2b)]-9b 
 \result{- 18 b}
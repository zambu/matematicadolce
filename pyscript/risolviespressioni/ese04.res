\item \(-{\dfrac{1}{4}}\tonda{2 abx+2a^{2}b^{2}+3ax} 
+a^{2}(b^{2}+x^{2})-\quadra{\tonda{\dfrac{1}{3} ax}^{2} 
-\tonda{\dfrac{2}{3}bx}^{2}}+
\dfrac{1}{4}ax\tonda{2b+3}\) 
 \result{\dfrac{a^{2} b^{2}}{2} + \dfrac{8 a^{2} x^{2}}{9} + \dfrac{4 b^{2} x^{2}}{9}}
#!/usr/bin/python3
# -*- coding: utf-8 -*-
#-------------------------------python----------------calclett_espr.py--#
#                                                                       #
#                              Polinomi                                 #
#                                                                       #
#--Daniele Zambelli--------------GPL------------------------------2022--#

"""
- Legge un file <nomefile>.tex
- Estrae le espressioni: "\(<espressione>\)"
- Ne calcola il risultato
- Scrive, espressioni e soluzioni nel file  <nomefile>.res
"""

import re
import sys, getopt
import os

import sympy as sp
from sympy.parsing.latex import parse_latex
from sympy.parsing.sympy_parser import (parse_expr,
                                        standard_transformations,
                                        implicit_multiplication)
transformations = standard_transformations + (implicit_multiplication,)

a, b, c, x, y, z, t = sp.symbols('a b c x y z t')

##exp = "ab (a^{2} - b^{2}) + 2b (x^{2} - a^{2})(a-b) - 2bx^{2} (a-b)"
##rawexpr = parse_latex(exp)
##expr = parse_expr(str(rawexpr), transformations=transformations)
##print('e:', exp)
##print()
##print('expr:', expr)
##print()
##print('latex simplify:\n', sp.latex(sp.simplify(expr)))
##print()
##print('latex expand:\n', sp.latex(sp.expand(expr)))
##print()
##print('wolframalpha:\n', "a^3 (-b) + 2 a^2 b^2 - a b^3")
##print()
##print()
##pippo

def closeblock(string, pos, opensymb='{', closesymb='}'):
    """Find position of corrispondent "}".

>>> closeblock('aaa{bbb}ccc', 3)
7
>>> closeblock('aaa{{b}{b{b}}}ccc', 3)
13
>>> closeblock('aaa{{b}{b{b}}ccc}', 3)
16
"""
    openg = 0
    i = pos
##    print(string[pos:])
    for c in string[pos:]:
##        print(c, i, string[i], openg)
        if c == opensymb:
            openg += 1
        elif c == closesymb:
            openg -= 1
            if openg == 0:
                break
        i += 1
##    print(pos, i, string[pos:i])
    return i

def transpar(exp, par, parsymb):
    """Translate:
\graffa{...}, \quadra{...}, \tonda{...} to \{...\}, [...], (...).

>>> exp = "3a\\quadra{2(a-2ab)+3a\\tonda{\\dfrac{1}{2}+3b}-\\dfrac{1}{2}a(3-5b)}"
>>> exp = transpar(exp, '\\tonda{', ('(', ')'))
>>> print(exp)
3a\quadra{2(a-2ab)+3a(\dfrac{1}{2}+3b)-\dfrac{1}{2}a(3-5b)}
>>> exp = transpar(exp, '\\quadra{', ('[', ']'))
>>> print(exp)
3a[2(a-2ab)+3a(\dfrac{1}{2}+3b)-\dfrac{1}{2}a(3-5b)]
>>> exp = transpar(exp, '\\graffa{', ('\\{', '\\}'))
>>> print(exp)
3a[2(a-2ab)+3a(\dfrac{1}{2}+3b)-\dfrac{1}{2}a(3-5b)]

>>> exp = "3\\tonda{x-\\dfrac{1}{3}y}\\quadra{2x+\\dfrac{1}{3}y-(x-2y)} - 2\\tonda{x-\\dfrac{1}{3}y}(2x+3y)"
>>> exp = transpar(exp, '\\tonda{', ('(', ')'))
>>> print(exp)
3(x-\dfrac{1}{3}y)\quadra{2x+\dfrac{1}{3}y-(x-2y)} - 2(x-\dfrac{1}{3}y)(2x+3y)
>>> exp = transpar(exp, '\\quadra{', ('(', ')'))
>>> print(exp)
3(x-\dfrac{1}{3}y)(2x+\dfrac{1}{3}y-(x-2y)) - 2(x-\dfrac{1}{3}y)(2x+3y)
>>> exp = transpar(exp, '\\graffa{', ('(', ')'))
>>> print(exp)
3(x-\dfrac{1}{3}y)(2x+\dfrac{1}{3}y-(x-2y)) - 2(x-\dfrac{1}{3}y)(2x+3y)

>>> exp = "3\\tonda{x-\\dfrac{1}{3}y}\\quadra{2x+\\dfrac{1}{3}y-(x-2y)}-2\\tonda{x-\\dfrac{1}{3}y}(2x+3y)"
>>> result = transpar(exp, '\\tonda{', ('(', ')'))
>>> print(result)
3(x-\dfrac{1}{3}y)\quadra{2x+\dfrac{1}{3}y-(x-2y)}-2(x-\dfrac{1}{3}y)(2x+3y)
>>> result = transpar(result, '\\quadra{', ('(', ')'))
>>> print(result)
3(x-\dfrac{1}{3}y)(2x+\dfrac{1}{3}y-(x-2y))-2(x-\dfrac{1}{3}y)(2x+3y)
>>> result = transpar(result, '\\graffa{', ('(', ')'))
>>> print(result)
3(x-\dfrac{1}{3}y)(2x+\dfrac{1}{3}y-(x-2y))-2(x-\dfrac{1}{3}y)(2x+3y)
"""
##    print('exp -------------->', exp)
    lenpar = len(par)
    endexp = len(exp)-lenpar
    findstart = 0
    chunks = []
    while findstart < endexp:
##        print(findstart)
        startpar = exp.find(par,  findstart)
        if startpar == -1:
            chunks += [exp[findstart:]]
            break
        chunks += [exp[findstart:startpar]]
        startpar += lenpar
##        print(startpar)
        endpar = closeblock(exp,  startpar-1)
        chunks += [parsymb[0], exp[startpar:endpar], parsymb[1]]
##        print(chunks)
        findstart = endpar + 1
    result = ''.join(chunks)
##    print('transpar:', result)
    return result

def substpar(exp):
    """Substitute:
\graffa{...}, \quadra{...}, \tonda{...} to \{...\}, [...], (...).

>>> exp = "3a\\quadra{2(a-2ab)+3a\\tonda{\\dfrac{1}{2}+3b}-\\dfrac{1}{2}a(3-5b)}"
>>> result = substpar(exp)
>>> print(result)
3a(2(a-2ab)+3a(\dfrac{1}{2}+3b)-\dfrac{1}{2}a(3-5b))

>>> exp = "3\\tonda{x-\\dfrac{1}{3}y}\\quadra{2x+\\dfrac{1}{3}y-(x-2y)}-2\\tonda{x-\\dfrac{1}{3}y}(2x+3y)"
>>> result = substpar(exp)
>>> print(result)
3(x-\dfrac{1}{3}y)(2x+\dfrac{1}{3}y-(x-2y))-2(x-\dfrac{1}{3}y)(2x+3y)
"""    
##    print(exp)
##    exp = transpar(exp, r'\tonda{', ('(', ')'))
##    exp = transpar(exp, r'\quadra{', ('[', ']'))
##    exp = transpar(exp, r'\graffa{', ('\\{', '\\}'))
    result = transpar(exp, '\\tonda{', ('(', ')'))
##    print(result)
    result = transpar(result, '\\quadra{', ('(', ')'))
##    print(result)
    result = transpar(result, '\\graffa{', ('(', ')'))
##    print(result)
##    pippo
    return result

##exp = r"\graffa{\quadra{\tonda{\dfrac{1}{2}}x+\dfrac{1}{3}}x+\dfrac{1}{4}}x+\dfrac{1}{5}"
##
##print(exp, "\n", substpar(exp))
##
##pippo

def extarctexpressions(ftext):
    endtext = len(ftext)-2
    findstart = 0
    expressions = []
    while findstart < endtext:
        startexp = ftext.find(r'\(',  findstart)
##        print(startexp)
        if startexp == -1:
            break
        endexp = ftext.find(r'\)',  startexp)
        if endexp == -1:
            print("ERRORE! ultima formula malformata.")
            endexp = endtext + 2
##        print(ftext[startexp+2:endexp])
        expressions.append(ftext[startexp+2:endexp])
        findstart = endexp+2
    return expressions

def lettpart(gra, lett='x'):
    if gra == 0:
        return ''
    if gra == 1:
        return 'x'
    else:
        return f'x^{{{gra}}}'

def poly_latex(poly):
    """Bello, peccato che funzioni solo per polinomi in x!"""
    coeffs = poly.all_coeffs()
    print(coeffs)
    gra = len(coeffs)-1
    print(gra)
    terms = []
    first = True
    for c in coeffs:
        if c != 0:
            if c > 0 and not first:
                sign = ' + '
            else:
                sign = ' '
            terms.append(sign + sp.latex(c) + lettpart(gra))
        gra -= 1
        first = False
    return ''.join(terms)

def result(latexexp):
    exp = substpar(latexexp)
    exp = exp.replace(r'\dfrac', r'\frac')
    rawexpr = parse_latex(exp)
    expr = parse_expr(str(rawexpr), transformations=transformations)
    print('latexexp:', latexexp)
    print('exp:', exp)
    print('rawexpr:', rawexpr)
    print('expr:', expr)
##    print('latex(expr):', sp.latex(expr))
##    print('latex simplify ---->', sp.latex(sp.simplify(expr)))
##    print('latex expand ---->', sp.latex(sp.expand(expr)))
##    return sp.latex(sp.simplify(sp.expand(expr)))
##    return sp.latex(sp.expand(sp.simplify(expr)))
##    return sp.latex(sp.simplify(expr))
##    return sp.latex(sp.expand(expr))
##    print(sp.exp(sp.Poly(expr)))
##    exprpoly = expr.as_poly()
##    coeffs = exprpoly.all_coeffs() # multivariate polynomials not supported
##    print(exprpoly.all_coeffs())
##    print(sp.latex(coeffs[0]))
##    print(poly_latex(expr.as_poly()))
####    print((sp.Poly(expr)).as_expr())
##    return sp.Poly(expr)
##    result = sp.latex(sp.Poly(expr).as_expr()).replace(r'\frac', r'\dfrac')
    result = sp.latex(sp.expand(expr)).replace(r'\frac', r'\dfrac')
    return result
    
def translate(ftext):
##    print(ftext)
    expressions = extarctexpressions(ftext)
    print(len(expressions))
    expres = [(exp, result(exp)) for exp in expressions]
##    print(expres)
    newline = "\n"
    ret = newline.join(
        [rf"\item \({exp}\) {newline} \result{{{res}}}" for exp, res in expres])
##    print(ret)
    return ret

def parse_ese(nfile):
    """Substitute nfile with translate(nfile)"""
    print('\n', nfile, '\n')
    namef, ext = os.path.splitext(nfile)
    with open(nfile) as f:
        ftext = f.read()
    ftext = translate(ftext)
    with open(namef+'.res', "w") as f:
        f.write(ftext)

def main(argv):
    inputfile = ''
    outputfile = ''
    try:
        opts, nfiles = getopt.getopt(argv,"hti:o:",["ifile=","ofile="])
##        print(opts, args)
    except(getopt.GetoptError):
        print(__doc__)
        sys.exit(2)
    ptest = False
    for opt, arg in opts:
        if opt == '-h':
             print(__doc__)
             sys.exit()
    if nfiles == []:
        nfiles = [os.path.join(dp, f) for
                  dp, dn, fn in os.walk('./') for f in fn if f.endswith('.tex')]
##        nfiles = [n_f for n_f in os.listdir('.') if n_f.endswith('.tex')]
    for filename in nfiles:
        parse_ese(filename)

TEST = True
##TEST = False
if __name__ == '__main__':
    if TEST:
        import doctest
        OPTFLG = doctest.NORMALIZE_WHITESPACE|doctest.ELLIPSIS
        doctest.testmod(optionflags=OPTFLG, verbose=True)
    else:
        main(sys.argv[1:])


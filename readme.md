## Matematica dolce

*Testo di matematica* 

- *libero*
- *collaborativo*
- *evolutivo*
- *polimorfo*
- *accessibile*

*per le scuole di secondo grado*

## Origine

Questo lavoro è partito da Matematica $C^3$ il testo libero era edito da

[matematicamente.it](www.matematicamente.it)

Lo si può ancora trovare in:

[www.matematicamente.it/manuali-scolastici](https://www.matematicamente.it/manuali-scolastici/)

o in:

[wikiversity](https://it.wikiversity.org/wiki/Corso:Liceo_scientifico)

Dopo alcuni anni di uso nel liceo economico sociale e nel liceo classico, 
il testo si sta diffondendo anche ad altri indirizzi scolastici. 
Così il progetto è stato ristrutturato in modo da facilitare la 
realizzazione di testi diversi a partire da un deposito di materiale. 

Il testo si è ampliato coprendo tutti gli argomenti presentati nella 
scuola secondaria di secondo grado.

Da alcuni anni sono stati prodotti e sperimentati in classe i 5 volumi 
che coprono tutti gli argomenti, dai *numeri naturali* agli *integrali*.

## Aspetti didattici

Il testo è frutto dell'esperienza didattica in classe e si è modificato 
in base all'esperienza di chi lo ha usato.

Non è una copia dei libri stampati dalle case editrici.
Molti argomenti sono presentati con metodi *strani*, non riscontrabili 
nei libri che si rivolgono alla *moda* degli insegnanti.
Metodi che sono frutto della ricerca del modo migliore di presentare 
un argomento; modo che non sempre coincide con quello più diffuso.

Una presentazione di alcuni di questi metodi strani la si può trovare in:

[www.matematicadolce.eu](https://www.matematicadolce.eu/)

In particolare nella sezione "Perché?".

Nello stesso sito, ma nella sezione "Varie" si può trovare una 
presentazione degli aspetti non matematici del progetto.

## Collaborazione

Perché questo progetto non muoia c'è bisogno anche del tuo aiuto!

Si può collaborare in vari modi:

- Usare il testo / adottarlo: il libro è vivo solo se viene usato.
- Correggerlo: il libro è vivo se vengono segnalati e corretti gli errori.
- Adattarlo: il libro è vivo se si adatta a diversi ambienti e a 
nuove situazioni.
- Ampliarlo:  il libro è vivo se cresce.
- Renderlo più accessibile: l’inclusività è uno degli obiettivi
fondanti del progetto.

Chiunque abbia materiale, idee, tempo da offrire per contribuire 
all'opera, e anche osservazioni e critiche, è pregato di contattare 
il coordinatore del progetto all'indirizzo sotto riportato.

In particolare le segnalazioni di errori e le proposte di miglioramento
possono essere fatte partendo dalla pagina:
[issues](https://bitbucket.org/zambu/matematicadolce/issues?status=new&status=open)
compilando il form che si apre cliccando sul pulsante in alto a destra: 
*create issue*.

## Come modificare il testo

Si può partire operando un fork del progetto o clonandolo in locale.

Questo progetto, col passare degli anni è diventato piuttosto complesso:

- Sono stati aggiunti diversi argomenti e diverse versioni dello stesso
argomento.
- Si possono creare i libri pescando da un deposito di materiali già 
scritti.
- Si possono creare diverse versioni dei volumi adattandoli alla 
programmazione della propria scuola.
- È in corso una ristrutturazione di tutto il materiale per produrre anche
le versioni html-mathjax e html-mathml per rendere i testi accessibili 
a chi ha bisogno di un lettore di schermo o di una barra Braille. 

Questa complessità del lavoro si riflette in una complessità di 
organizzazione.

### Struttura delle directory

Di seguito descrivo sommariamente lo stato attuale delle directory con 
alcuni brevi commenti.

Più sotto riprendo i commenti segnati con un asterisco.

```
.
├── cantiere               <- assemblaggio libri*
│   ├── cfp                <- libro per un centro di form. professionale
│   │   └── v_1
│   │       └── ...
│   ├── ips                <- libro per un ist. di formazione professionale
│   │   ├── copertine      <- copertina del testo
│   │   │   └── ...
│   │   └── v_1            <- contenuto del testo
│   │       └── ...
│   └── licei              <- testi per i licei non scientifici
│       ├── copertine      <- copertine
│       │   ├── calcoli_copertine.ods    <- calcoli per le copertine*
│       │   ├── img        <- immagini delle copertine
│       │   │   └── ...
│       │   ├── m_d_licei_21   <- copertine ultima edizione
│       │       ├── m_d_licei_21_1_copertina.pdf
│       │       ├── m_d_licei_21_1_copertina.svg
│       │       └── ...
│       ├── geo_raz        <- testo di geometria
│       │   └── ...
│       ├── nsa            <- testo di analisi non standard
│       │   └── ...
│       ├── v_1            <- primo volume*
│       │   ├── creapdf.py -> ../../../creapdf.py
│       │   ├── deposito -> ../../../deposito
│       │   ├── Makefile -> ../../../Makefile
│       │   ├── m_d_licei_19_1.tex    <- edizione 2019
│       │   ├── ...
│       │   ├── m_d_licei_22_1.tex    <- edizione 2021
│       │   └── md_make4ht.cfg -> /.../matematicadolce/md_make4ht.cfg
│       ├── v_2            <- secondo volume
│       │   └── ...
│       ├── v_3            <- terzo volume
│       │   └── ...
│       ├── v_4            <- quarto volume
│       │   └── ...
│       └── v_5            <- quinto volume
│           └── ...
├── creapdf.py             <- script Python per la compilazione dei testi*
├── deposito               <- tutti i contenuti*
│   ├── licenze      
│   │   ├── CC-BY-SA-3-0
│   │   └── LPPL-1-3c
│   ├── magazzino          <- Contiene strumenti di uso generale*
│   │   ├── assiepiani_datogliere   <- obsoleto
│   │   │   └── ...
│   │   ├── definizioni.tex         <- funzioni usate nei vari capitoli
│   │   ├── definizioni_tikz.tex    <- funzioni grafiche usate nei vari cap.
│   │   ├── img
│   │   │   └── by-sa.png
│   │   ├── matsweetmem.cls         <- classe base del testo
│   │   ├── matsweet.sty            <- pacchetto base del testo
│   │   ├── packages.tex            <- pacchetti LaTeX usati
│   │   └── variabili.tex           <- le variabili generali
│   └── materiali
│       ├── 00_intestazioni         <- intestazioni usate nei diversi volumi
│       │   ├── cfp 
│       │   │   └── prefazione.tex
│       │   ├── geometria_razionale
│       │   │   ├── ...
│       │   │   └── prefazione.tex
│       │   └── les                 <- intest. per liceo economico sociale
│       │       ├── colophon.tex
│       │       ├── frontespizio.tex
│       │       ├── indice.aux
│       │       ├── indice.tex
│       │       └── prefazione.tex
│       ├── 01_Logica
│       │   ├── 01_Insiemi_logica 
│       │   │   └── ins01           <- insiemi
│       │   │       ├── insiemi_ese.tex          <- esercizi
│       │   │       ├── insiemi_grafici.tex      <- alcuni grafici
│       │   │       ├── insiemi.tex              <- teoria
│       │   │       └── lbr                      <- altri grafici
│       │   │           └── ...
│       │   ├── 02_Relazioni
│       │   │   └── rel01           <- relazioni e funzioni
│       │   └── 03_Funzioni
│       │       └── funz01          <- funzioni versione "classica"
│       │           ├── funzioni_ese.tex
│       │           └── funzioni.tex
│       ├── 02_Insiemi_numerici   
│       │   ├── 01_Numeri_naturali
│       │   │   └── nat01
│       │   │       ├── img                      <- immagini per nat01
│       │   │       │   └── ...
│       │   │       ├── naturali_ese.tex         <- esercizi
│       │   │       ├── naturali_grafici.tex     <- grafici
│       │   │       └── naturali.tex             <- teoria
│       │   ├── 02_Numeri_interi
│       │   │   └── int01
│       │   │       └── ...
│       │   ├── 03_Numeri_razionali
│       │   │   ├── raz01                        <- versione precedente
│       │   │   │   ├── img
│       │   │   │   │   └── ...
│       │   │   │   ├── lbr
│       │   │   │   │   └── ...
│       │   │   │   ├── razionali_ese.tex
│       │   │   │   ├── razionali.tex
│       │   │   │   └── reali.tex
│       │   │   └── raz02                        <- versione attuale
│       │   │       ├── img
│       │   │       │   ├── eyefra.png
│       │   │       │   ├── eye.png
│       │   │       │   └── giero.png
│       │   │       ├── razionali_ese.tex
│       │   │       ├── razionali_grafici.tex
│       │   │       ├── razionali.tex
│       │   │       └── reali.tex
│       │   ├── 05_Numeri_reali
│       │   │   └── re01
│       │   │       └── ...
│       │   └── 07_Numeri_iperreali
│       │       ├── iperr01
│       │       │   ├── img
│       │       │   │   ├── fractal.jpg
│       │       │   │   ├── fractal.txt
│       │       │   │   ├── Mandelbrot_Set-10-SATELLITE_VALLEY-large.jpg
│       │       │   │   └── Mandelbrot.txt
│       │       │   ├── iperreali_ese.tex
│       │       │   ├── iperreali_grafici.tex
│       │       │   ├── iperreali.tex
│       │       │   ├── naturali_iperreali_ese.tex
│       │       │   ├── naturali_iperreali_grafici.tex
│       │       │   └── naturali_iperreali.tex
│       │       └── iperr02
│       │           └── iperreali_ese.tex
│       ├── 03_Polinomi
│       │   ├── 01_Monomi_polinomi
│       │   │   └── calclett01
│       │   ├── 02_Divisibilita
│       │   │   └── poldiv01
│       │   ├── 03_Frazioni_algebriche
│       │   │   └── frazalg01
│       │   └── 05_Radicali
│       │       └── rad01
│       ├── 04_Equazioni
│       │   ├── 01_Eq_grado_1
│       │   │   ├── compl01
│       │   │   ├── eq01
│       │   │   └── probl01
│       │   ├── 02_Diseq_grado_1
│       │   │   └── dis01
│       │   ├── 03_Sistemi_lineari
│       │   │   └── sist01
│       │   ├── 04_Eq_grado_2
│       │   │   └── eq201
│       │   ├── 05_Diseq_grado_2
│       │   │   └── dis201
│       │   └── 06_Grado_2+
│       │       └── compl201
│       ├── 05_Geometria_euclidea
│       │   ├── 01_Assiomi_teoremi
│       │   │   ├── fond01          <- fondamenti di geometria
│       │   │   │   └── src         <- sorgenti degli script Python
│       │   │   └── fond02          <- fondamenti di geom. altra versione
│       │   │       └── ...
│       │   ├── 02_Triangoli_rette
│       │   │   ├── parall01        <- parallelogrammi
│       │   │   │   └── ...
│       │   │   ├── parall02        <- altra versione
│       │   │   │   └── ...
│       │   │   ├── quadr01         <- quadrilateri
│       │   │   │   └── ...
│       │   │   ├── quadr02         <- quadrilateri altra versione
│       │   │   │   └── ...
│       │   │   ├── tri01           <- triangoli
│       │   │   │   └── ...
│       │   │   └── tri02           <- triangoli altra versione
│       │   │   │   └── ...
│       │   ├── 03_Circonferenza
│       │   │   ├── circ01
│       │   │   │   └── ...
│       │   │   └── circ02
│       │   │       └── ...
│       │   └── 04_Equiestensione
│       │       ├── equie01
│       │       │   └── ...
│       │       ├── equie02
│       │       │   └── ...
│       │       ├── prop01          <- proporzionalità
│       │       │   └── ...
│       │       └── prop02          <- proporzionalità altra versione
│       │           └── ...
│       ├── 06_Trasformazioni
│       │   ├── 01_Trasf_geometriche
│       │   │   └── trasf01
│       │   │       └── trasformazioni.tex
│       │   └── 02_Isometrie
│       │       └── isom01
│       │           ├── img
│       │           │   └── ...
│       │           ├── isometrie1_ese.tex
│       │           ├── isometrie1.tex
│       │           ├── isometrie_ese.tex        <- versione precedente
│       │           ├── isometrie.tex            <- versione precedente
│       │           ├── lbr                      <- grafici    
│       │           │   └── ...
│       │           └── src                      <- sorgenti Python
│       │               └── ...
│       ├── 07_Geometria_analitica
│       │   ├── 01_Punti_segmenti
│       │   │   └── ...
│       │   ├── 02_Retta
│       │   │   └── ...
│       │   ├── 03_Parabola
│       │   │   └── ...
│       │   ├── 04_Circonferenza
│       │   │   └── ...
│       │   ├── 05_Ellisse_iperbole
│       │   │   ├── ell01
│       │   │   │   └── ...
│       │   │   └── iper01
│       │   │       └── ...
│       │   ├── 06_Luoghi_geometrici
│       │   │   └── luo01
│       │   │       └── ...
│       │   └── 08_Geometria_3d
│       │       └── g3d01
│       │           └── ...
│       ├── 08_Algebra
│       │   └── 01_Vettori_matrici
│       │       └── vett01
│       │           └── ...
│       ├── 09_Goniometria
│       │   ├── 01_Funzioni_goniometriche
│       │   │   └── gonio01
│       │   │       └── ...
│       │   └── 03_Trigonometria
│       │       └── trigo01
│       │           └── ...
│       ├── 0_a_parti               <- sudd. in parti dei primi volumi
│       │   └── ...
│       ├── 10_Esp_log              <- esponenziali e logaritmi
│       │   └── 01_Funz_eq_diseq
│       │       └── ...
│       ├── 12_Analisi
│       │   ├── 01_Topologia_Funzioni
│       │   │   ├── funzioni01                   <- funzioni vers. classica
│       │   │   │   └── ...
│       │   │   ├── funzioni02                   <- funzioni versione strana
│       │   │   │   ├── funzioni_ese.tex
│       │   │   │   ├── funzioni_grafici.tex
│       │   │   │   ├── funzioni.tex
│       │   │   │   └── src
│       │   │   │       └── ...
│       │   │   └── topologia01                  <- topologia della retta
│       │   │       └── ...
│       │   ├── 02_Limiti
│       │   │   └── limiticontinuita             <- continuità e limiti
│       │   │       └── ...
│       │   ├── 03_Funzioni_continue
│       │   │   └── teoremicontinuita            <- teoremi sulle f continue
│       │   │       └── ...
│       │   ├── 04_Derivata
│       │   │   └── diff01
│       │   │       └── ...
│       │   ├── 05_Studio_funzione
│       │   │   └── studiof01
│       │   │       └── ...
│       │   ├── 06_Integrali
│       │   │   └── int01
│       │   │       └── ...
│       │   └── 09_Calcolo_numerico
│       │       └── calcnum01
│       │           └── ...
│       ├── 14_Statistica
│       │   └── 01_Statistica_descrittiva
│       │       ├── stat01
│       │       │   └── ...
│       │       └── stat02
│       │           └── ...
│       ├── 15_Probabilita
│       │   ├── 01_Calcolo_combinatorio
│       │   │   └── comb01
│       │   │       └── ...
│       │   ├── 02_Calcolo_probabilita
│       │   │   ├── prob01
│       │   │   │   └── ...
│       │   │   └── prob02
│       │   │       └── ...
│       │   └── 03_Distribuzioni_probabilita
│       │       └── ...
│       ├── 16_Economia
│       │   ├── 01_Matematica_finanziaria
│       │   │   └── finanz01
│       │   │       └── ...
│       │   └── 02_Modelli_economici
│       │       ├── modec01
│       │       │   └── ...
│       │       └── modec02
│       │           └── ...
│       └── 17_Strumenti_digitali
│           ├── 01_Foglio_calcolo
│           │   └── calc01
│           │       └── ...
│           └── 02_Python
│               └── 02_Geo_inter
│                   └── geoint01
│                       └── ...
├── .git
├── .gitignore            <- indica quali file git deve ignorare
├── Makefile              <- altro modo per produrre i pdf
├── md_make4ht.cfg        <- file di configurazione per make4ht
├── pyscript              <- script per semplificare la scrittura
│   ├── interi_esp.py
│   ├── interi_tabelle.py
│   └── razionali_tabelle.py
├── readmec3.txt          <- readme del progetto Matematica C3
├── readme.md             <- questo file
└── varie                 <- materiali correlati al progetto
│   └── mathesisvr21      <- presentazione di Matematica Dolce al congresso
│       └── ...

```

### cantiere

È la zona dove si costruiscono i testi.

La directory ```cantiere``` contiene i file che descrivono i testi e i 
risultati della compilazione.

Questa directory ha delle sottodirectory per i diversi tipi di scuola 
che a loro volta ne contengono una per le copertine e una per ogni 
volume.

### calcoli_copertine.ods

È un foglio di calcolo che fornisce le posizioni dei diversi elementi 
della copertina in funzione del numero di pagine del volume.

### v_1 (v_2, ...)

Contiene link a vari file di utilità e il file:

```m_d_licei_22_1.tex```

che è il file ```tex``` da compilare per produrre il 
volume **1** dell'edizione **2022** per i **licei**.

Il file può essere compilato con il comando:

```pdflatex --shell-escape m_d_licei_22_1.tex```

oppure usando lo script ```creapdf.py```, script che illustro qui sotto.

### creapdf.py

È uno script Python che semplifica la compilazione in 
```pdf```, ```html+mathjax```, ```html+mathml```.

Con il comando:

```$> ./creapdf.py -h```

Si ottiene il seguente risultato:

```
Uso:

# trasforma in pdf tutti i file .tex presenti nella directory e
  produce pdf monocromatici:
$> ./creapdf.py

# trasforma in pdf i file .tex passati come argomento e
  produce un pdf monocromatico:
$> ./creapdf.py pippo.tex pluto.tex

# opzione -t (test) fa una sola compilazione,
  non cancella i file ausiliari e non produce il pdf monocromatico:
$> ./creapdf.py -t [<elencofile>]

# opzione -x (xhtml) produce un file xhtml con mathml:
$> ./creapdf.py -x [<elencofile>]

# opzione -j (xhtml) produce un file xhtml con matjax:
$> ./creapdf.py -x [<elencofile>]

# opzione -m (mono) esegue la compilazione e produce il pdf monocromatico:
$> ./creapdf.py -m

# opzione -c (clean) pulisce le directory dai file di supporto:
$> ./creapdf.py -c

```

### deposito

Contiene tutti i file LaTeX necessari per produrre i contenuti.

### magazzino

Contiene strumenti LaTeX che vengono usati nel testo:

- pacchetto dedicato,
- preambolo,
- definizioni di testo e matematiche,
- definizioni grafiche,
- variabili.

### materiali
Contiene tutti i capitoli.
È suddivisa per aree matematiche e alcuni capitoli presentano più versioni.

## Per compilare un volume

Aprire un terminale nella directory del volume che si vuole compilare.
Ad es:

```$> cd cantiere/licei/v_1```

Compilare la versione 2022 in modalità *test* cioè con un'unica 
compilazione:

```$> ./creapdf.py -t m_d_licei_22_1.tex```

Per ottenere i riferimenti corretti e l'indice bisogna ripetere la 
compilazione più volte.

Questa modalità è quella più usata quando si sta modificando un testo o 
un capitolo.

Per produrre ```html+mathjax```:

```$> ./creapdf.py -j m_d_licei_22_1.tex```

Per produrre ```html+mathml```:

```$> ./creapdf.py -x m_d_licei_22_1.tex```

Per ripulire dai file prodotti dalla compilazione:

```$> ./creapdf.py -c```

Per compilare la versione ```pdf``` definitiva (due compilazioni LaTeX e 
produzione del file monocromatico):

```$> ./creapdf.py m_d_licei_22_1.tex```

## Modifica

Si possono effettuare modifiche a diversi livelli.

### Modifica di un volume

Modificare con un qualunque editor il file ```m_d_licei_22_1.tex```, 
permette di cambiare:

- il titolo,
- l'ordine dei capitoli,
- quali capitoli inserire nel testo.

Se commento tutte le chiamate ai vari capitoli salvo una, la compilazione 
produrrà una dispensa con solo i contenuti di quel capitolo.
In questo caso potrà essere utile modificare il titolo e commentare anche 
la chiamata alla prefazione.

### Modifica di un capitolo

Un capitolo è di solito costituito da 3 file come ad esempio per 
il capitolo sui *numeri interi*:

- ```interi.tex```: la teoria,
- ```interi_ese.tex```: gli esercizi,
- ```interi_grafici.tex```: i grafici,

Modificati questi file, la compilazione produce il testo con il capitolo 
aggiornato alla nuova versione.

## Download

Dalla sezione download di questo repository:

[bitbucket.org/zambu/matematicadolce/downloads](https://bitbucket.org/zambu/matematicadolce/downloads/)

si può scaricare l'ultima versione dei 5 volumi in formato pdf e le loro 
copertine.

## Link

Tutto il lavoro è attualmente contenuto nei due siti:
[matematicadolce.eu](https://www.matematicadolce.eu/)
[bitbucket.org/zambu/matematicadolce](https://bitbucket.org/zambu/matematicadolce/)

## Licenza

Tutto il materiale di questo progetto è rilasciato sotto licenza: 
[CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/deed.it).

## Coordinatore del progetto

Indirizzo per comunicare con il coordinatore del progetto:
daniele.zambelli@gmail.com

---

Con la speranza che questo testo possa 
contribuire a far amare la matematica

Daniele
